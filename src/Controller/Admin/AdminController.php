<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class AdminController extends AbstractDashboardController
{
    
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $user =  $this->getUser();
        if($user->getRoles()[0] == "Role_Admin"){
             $routeBuilder = $this->container->get(AdminUrlGenerator::class);
        $url = $routeBuilder->setController(UserCrudController::class)->generateUrl();

        return $this->redirect($url);

        }
        if($user->getRoles()[0] == "Role_User"){
            return $this->redirect("/post");
        }
        
    }
 

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Projet Entraide Php');
    }

    public function configureMenuItems(): iterable
    {   
        yield MenuItem::linkToCrud('User', 'fas fa-comments', User::class);
        yield MenuItem::linkToCrud('Post', 'fas fa-comments', Post::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
