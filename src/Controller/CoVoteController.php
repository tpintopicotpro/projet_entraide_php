<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoVoteController extends AbstractController
{
    #[Route('/co/vote', name: 'app_co_vote')]
    public function index(): Response
    {
        return $this->render('co_vote/index.html.twig', [
            'controller_name' => 'CoVoteController',
        ]);
    }
}
