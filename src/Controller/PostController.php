<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Post;
use App\Form\PostFormType;
use App\Form\CommentaireType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\DateTimeImmutable;
use App\Entity\Commentaire;
use App\Entity\Vote;
use Symfony\Component\HttpFoundation\RedirectResponse;



class PostController extends AbstractController
{ 

    #[Route('/', name: 'app_post')]
    public function post(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        $post_home =  new Post();
        $user = $this->getUser();
        $form = $this->createForm(PostFormType::class, $post_home,[
            
        ]);
        $post_home -> setUserId($user);
        $form->handleRequest($request);
        
        
        if($form->isSubmitted() && $form->isValid()){
            $post_home->setCreatedAt(new DateTimeImmutable('Y-m-d'));// date actuelle automatisée au moment de l'envoie du commentaire.
            $entityManager->persist($post_home);
            $entityManager->flush();
        }
          

        $post = $doctrine->getRepository(Post::class)->findAll();
        // json_decode(json_encode($post), true);
       
        $post_reverse =  array_reverse($post);
        
        return $this->render('base.html.twig', [
            'controller_name' => 'Post',
            'data' => $post_reverse,
            'form' => $form -> createView()
        ]);
    }

    #[Route('/post', name: 'app_postUser')]
    public function showPost(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        
        $user = $this->getUser(); 
        $post = $doctrine->getRepository(Post::class)->findBy(['user_id' => $user->getId()]);
        return $this->render('post/post.html.twig', [
            'controller_name' => 'Post',
            'data' => $post,
        ]);
            
     
    }

    #[Route('/post/{id}', name: 'app_show')]
    public function showPostUser(int $id, ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        
        $commentaire =  new Commentaire();
        $user = $this->getUser(); 
        $post = $doctrine->getRepository(Post::class)->find($id);
        $vote = new Vote();
        $form = $this->createForm(CommentaireType::class, $commentaire,[
        ]);
        $form->handleRequest($request);
       
        if($form->isSubmitted() && $form->isValid()){
            //recuperer les Id user et id post 
            $date = new DateTimeImmutable('Y-m-d');
            $commentaire->setCreatedAt($date); 
            $commentaire -> setUserId($user);
            $commentaire -> setPostId($post);
            $entityManager->persist($commentaire);
            $entityManager->flush();
        }
          
                  
        $comment = $doctrine->getRepository(Commentaire::class)->findBy(['post_id' => $id]);
        $vote = $doctrine->getRepository(Vote::class)->findBy(['commentaire_id' => $id]);
        return $this->render('post/index.html.twig', [
            'controller_name' => 'Post',
            'data' => $post,
            'form' => $form -> createView(),
            'comment' => $comment,
        ]);
            
     
    }
    #[Route('/post/{id}/vote/{ID}', name: 'app_comm')]
    public function addVote(int $id, int $ID, ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        $comment = $doctrine->getRepository(Commentaire::class)->find($ID);
        $post = $doctrine->getRepository(Post::class)->find($id);
        $uservoted = $post->getVotes();
        $user = $this->getUser();
        $vote = new Vote();
        $change = false;
        foreach($uservoted as $item){
            if($item->getUserId() == $user){
                $change = true;
            }

        }
        if($change == false){
            $vote->setCommentaireId($comment); 
            $vote -> setUserId($user);
            $vote -> setPost($post);
            $entityManager->persist($vote);
            $entityManager->flush();
        }
        
        return new RedirectResponse('/');
    }

    #[Route('/post/{id}/delete', name: 'app_del')]
    public function delPostUser(int $id, ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        
        
        $user = $this->getUser(); 
        $post = $doctrine->getRepository(Post::class)->find($id);
        $user_post = $user->getPostId();
        $commentaire = $doctrine->getRepository(Commentaire::class)->findBy(['post_id' => $id]);
        $vote = $doctrine->getRepository(Vote::class)->findBy(['Post' => $id]);
        if($user->getId() && $post->getUserId()){
            //recuperer les Id user et id post 
            if($commentaire != []){
                $entityManager->remove($commentaire[0]);
            }
            
            $entityManager->remove($post);
            if($vote != []){
                $entityManager->remove($vote);
            }
            
            $entityManager->flush();
        }
        return new RedirectResponse('/post');
    
}
}
