<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Commentaire;
use App\Form\CommentaireFormType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class CommentaireController extends AbstractController
{ 

    #[Route('/commentaire', name: 'app_commentaire')]
    public function commentaire(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        $commentaire =  new Commentaire();

        $form = $this->createForm(PostFormType::class, $commentaire,[
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($commentaire);
            $entityManager->flush();
        }

        $commentaire = $doctrine->getRepository(Commentaire::class)->findAll();
        return $this->render('base.html.twig', [
            'controller_name' => 'Commentaire',
            'data' => $commentaire,
            'form' => $form -> createView()
        ]);

    }
}
