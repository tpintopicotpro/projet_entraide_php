<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ProfilType;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserController extends AbstractController
{
    #[Route('/profil', name: 'app_profil')]
    public function index(Request $request, UserPasswordHasherInterface $userPasswordHasher , EntityManagerInterface $entityManager): Response
    {
        $profil =  $this->getUser();
        return $this->renderForm('profil/profil.html.twig', [
            'profil' => $profil,
        ]);
    }
    #[Route('/profil/modif', name: 'app_modifs')]
    public function prof_modif(Request $request, UserPasswordHasherInterface $userPasswordHasher , EntityManagerInterface $entityManager): Response
    {
        $profil =  $this->getUser();
        // ...

        $form = $this->createForm(ProfilType::class, $profil,[
            
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $profil->setPassword(
                $userPasswordHasher->hashPassword(
                        $profil,
                        $form->get('password')->getData()
                    )
                );
            $entityManager->persist($profil);
            $entityManager->flush();
        }
        return $this->renderForm('profil/index.html.twig', [
            'form' => $form,
        ]);
    }
}
